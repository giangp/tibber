package la.me.leo.tibber.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<VS : BaseViewState>(initial: VS) : ViewModel() {

    protected val _viewStates: MutableLiveData<VS> = MutableLiveData(initial)

    val viewStates: LiveData<VS>
        get() = _viewStates

    protected val model = viewStates.value!!

}

interface BaseViewState
