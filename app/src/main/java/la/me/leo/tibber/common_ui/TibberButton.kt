package la.me.leo.tibber.common_ui

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.core.content.withStyledAttributes
import com.google.android.material.button.MaterialButton
import la.me.leo.tibber.R

const val PRIMARY = 0
const val SECONDARY = 1
const val NEGATIVE = 2

class TibberButton(context: Context, attrs: AttributeSet) : MaterialButton(context, attrs) {

    var type: Int = PRIMARY
        set(value) {
            field = value
            updateAppearance()
        }

    init {
        context.withStyledAttributes(attrs, R.styleable.TibberButton) {
            type = getInt(R.styleable.TibberButton_type, PRIMARY)
        }
        isAllCaps = false
        insetBottom = 0
        insetTop = 0

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        cornerRadius = measuredHeight / 2
    }

    private fun updateAppearance() {
        when (type) {
            PRIMARY -> {
                backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.tibber_200))
                strokeColor = null
                strokeWidth = 0
                setTextAppearance(R.style.TextTibber_Utility1_White)
                rippleColor = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.white))
            }
            SECONDARY -> {
                backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.white))
                strokeColor = null
                strokeWidth = 0
                setTextAppearance(R.style.TextTibber_Utility1_Grey700)
                rippleColor = ColorStateList.valueOf(ContextCompat.getColor(context, android.R.color.darker_gray))
            }
            NEGATIVE -> {
                backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, android.R.color.transparent))
                strokeColor = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.red))
                strokeWidth = dp(1, context)
                setTextAppearance(R.style.TextTibber_Utility1_Red)
                rippleColor = ColorStateList.valueOf(ContextCompat.getColor(context, android.R.color.darker_gray))
            }
        }
    }
}
