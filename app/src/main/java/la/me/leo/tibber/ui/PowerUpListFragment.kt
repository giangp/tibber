package la.me.leo.tibber.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import la.me.leo.tibber.MainActivity
import la.me.leo.tibber.base.StatefulFragment
import la.me.leo.tibber.databinding.FragmentPowerUpListBinding
import la.me.leo.tibber.presentation.PowerUpListVS
import la.me.leo.tibber.presentation.PowerUpListViewModel
import la.me.leo.tibber.ui.adapter.PowerUpAdapter
import la.me.leo.tibber.ui.misc.PowerUpListItemDivider
import org.koin.androidx.viewmodel.ext.android.viewModel

internal class PowerUpListFragment : StatefulFragment<PowerUpListViewModel, PowerUpListVS, FragmentPowerUpListBinding>() {

    override val viewModel: PowerUpListViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.listPowerUps.run {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    override fun render(viewState: PowerUpListVS) {
        when (viewState) {
            PowerUpListVS.Failure -> {

            }
            is PowerUpListVS.ListLoaded -> {
                val itemDivider = PowerUpListItemDivider(requireContext())
                val listAdapter = PowerUpAdapter {
                    (activity as MainActivity).navigateToPowerUpDetail(it.powerUp)
                }
                listAdapter.items = viewState.items
                binding.listPowerUps.run {
                    adapter = listAdapter
                    addItemDecoration(itemDivider)
                }
            }
            PowerUpListVS.LoadingList -> {

            }
        }
    }

    override fun createView(inflater: LayoutInflater, container: ViewGroup?): FragmentPowerUpListBinding {
        return FragmentPowerUpListBinding.inflate(inflater, container, false)
    }
}
