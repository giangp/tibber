package la.me.leo.tibber.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import coil.load
import coil.transform.RoundedCornersTransformation
import la.me.leo.tibber.R
import la.me.leo.tibber.base.BaseFragment
import la.me.leo.tibber.common_ui.NEGATIVE
import la.me.leo.tibber.common_ui.PRIMARY
import la.me.leo.tibber.databinding.FragmentPowerUpDetailBinding
import la.me.leo.tibber.domain.models.PowerUp


private const val ARG_POWER_UP = "arg_power_up"

internal class PowerUpDetailFragment private constructor() : BaseFragment<FragmentPowerUpDetailBinding>() {

    private lateinit var powerUp: PowerUp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        powerUp = arguments?.getParcelable(ARG_POWER_UP)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            tvTitle.text = powerUp.title
            tvShortDesc.text = powerUp.description
            tvDesc.text = powerUp.longDescription
            ivPowerUp.load(powerUp.imageUrl) {
                crossfade(true)
                transformations(RoundedCornersTransformation(requireContext().resources.getDimension(R.dimen.u1)))
            }
            val (text, type) = if (powerUp.connected) {
                getString(R.string.connect_to_tibber) to PRIMARY
            } else {
                getString(R.string.disconnect_from_tibber) to NEGATIVE
            }
            btnConnection.type = type
            btnConnection.text = text
            toolbar.title = powerUp.title
            toolbar.setNavigationIcon(R.drawable.ic_back)
            toolbar.setNavigationOnClickListener {
                activity?.onBackPressed()
            }
            tvDescTitle.text = getString(R.string.more_about, powerUp.title)
            btnBuy.setOnClickListener {
                try {
                    val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(powerUp.storeUrl))
                    startActivity(myIntent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.no_browser_error_message),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun createView(inflater: LayoutInflater, container: ViewGroup?): FragmentPowerUpDetailBinding {
        return FragmentPowerUpDetailBinding.inflate(inflater, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(powerUp: PowerUp) = PowerUpDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_POWER_UP, powerUp)
            }
        }
    }
}
