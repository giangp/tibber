package la.me.leo.tibber.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PowerUp(
    val title: String,
    val description: String,
    val longDescription: String,
    val connected: Boolean,
    val storeUrl: String,
    val imageUrl: String
): Parcelable
