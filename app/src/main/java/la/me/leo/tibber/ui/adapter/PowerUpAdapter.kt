package la.me.leo.tibber.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import la.me.leo.tibber.R
import la.me.leo.tibber.databinding.ItemPowerUpListHeaderBinding
import la.me.leo.tibber.databinding.ItemPowerUpListPowerUpBinding
import la.me.leo.tibber.presentation.models.Header
import la.me.leo.tibber.presentation.models.PowerUp
import la.me.leo.tibber.presentation.models.PowerUpListItemModel
import la.me.leo.tibber.ui.adapter.PowerUpAdapter.ViewHolder

private const val POWER_UP_TYPE = 0
private const val HEADER_TYPE = 1

class PowerUpAdapter(
    private val clickListener: (PowerUp) -> Unit
) : RecyclerView.Adapter<ViewHolder<out PowerUpListItemModel>>() {

    var items: List<PowerUpListItemModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<out PowerUpListItemModel> {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            HEADER_TYPE -> HeaderViewHolder(ItemPowerUpListHeaderBinding.inflate(inflater, parent, false))
            POWER_UP_TYPE -> {
                PowerUpViewHolder(
                    ItemPowerUpListPowerUpBinding.inflate(inflater, parent, false),
                    clickListener = clickListener
                )
            }
            else -> throw IllegalStateException("PowerUpAdapter only supports HEADER_TYPE or POWER_UP_TYPE")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder<out PowerUpListItemModel>, position: Int) {
        when (holder) {
            is HeaderViewHolder -> holder.bind(items[position] as Header)
            is PowerUpViewHolder -> holder.bind(items[position] as PowerUp)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is Header -> HEADER_TYPE
            is PowerUp -> POWER_UP_TYPE
        }
    }

    sealed class ViewHolder<T: PowerUpListItemModel>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T)
    }

    class PowerUpViewHolder(
        private val binding: ItemPowerUpListPowerUpBinding,
        private val clickListener: (PowerUp) -> Unit
    ) : ViewHolder<PowerUp>(binding.root) {

        lateinit var item: PowerUp

        init {
            binding.root.setOnClickListener {
                clickListener(item)
            }
        }

        override fun bind(item: PowerUp) {
            this.item = item
            binding.ivPowerUp.load(item.powerUp.imageUrl) {
                crossfade(true)
                transformations(RoundedCornersTransformation(itemView.context.resources.getDimension(R.dimen.u1)))
            }
            binding.tvTitle.text = item.powerUp.title
            binding.tvShortDesc.text = item.powerUp.description
        }
    }

    class HeaderViewHolder(private val binding: ItemPowerUpListHeaderBinding) : ViewHolder<Header>(binding.root) {
        override fun bind(item: Header) {
            binding.tvHeader.text = item.text
        }
    }
}
