package la.me.leo.tibber.ui.misc

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.applyDimension
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import la.me.leo.tibber.R
import la.me.leo.tibber.common_ui.dp


class PowerUpListItemDivider(context: Context) : ItemDecoration() {

    private val dividerColor = ContextCompat.getColor(context, R.color.grey_400)
    private val paint = Paint()
    private val dividerHeight = dp(1, context)
    private val inset = context.resources.getDimension(R.dimen.u2)

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val childCount = parent.childCount
        (0 until childCount - 1).map { parent.getChildAt(it) }
            .forEach { view ->
                val position = parent.getChildAdapterPosition(view)
                val typeCurr = parent.adapter!!.getItemViewType(position)
                val typeNext = parent.adapter!!.getItemViewType(position + 1)
                if (typeCurr == typeNext) {
                    drawDivider(c, view.bottom.toFloat())
                }
            }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = dividerHeight
        }
        outRect.bottom = dividerHeight
    }

    private fun drawDivider(canvas: Canvas, y: Float) {
        paint.color = dividerColor
        canvas.drawRect(inset, y, canvas.width.toFloat() - inset, y + dividerHeight, paint)
    }

}
