package la.me.leo.tibber.domain

import com.apollographql.apollo.ApolloClient
import la.me.leo.tibber.domain.models.PowerUp
import la.me.leo.tibber.remote.queryResult
import la.me.leo.tibber.queries.PowerUpsQuery

interface FetchPowerUpUseCase {
    suspend fun execute(): List<PowerUp>
}

// for a relatively simple assignment, no caching required, skip repository layer and directly inject remote client
internal class FetchPowerUpUseCaseImpl(private val client: ApolloClient): FetchPowerUpUseCase {
    override suspend fun execute(): List<PowerUp> {
        val list = client.queryResult(PowerUpsQuery()).assignmentData ?: emptyList()
        return list.map {
            with(it) { PowerUp(title, description, longDescription, connected, storeUrl, imageUrl) }
        }
    }

}
