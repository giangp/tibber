package la.me.leo.tibber.presentation.models

import la.me.leo.tibber.domain.models.PowerUp

sealed class PowerUpListItemModel

data class PowerUp(val powerUp: PowerUp): PowerUpListItemModel()
data class Header(val text: String): PowerUpListItemModel()
