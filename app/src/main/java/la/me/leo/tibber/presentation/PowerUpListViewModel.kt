package la.me.leo.tibber.presentation

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import la.me.leo.tibber.R
import la.me.leo.tibber.base.BaseViewModel
import la.me.leo.tibber.base.BaseViewState
import la.me.leo.tibber.common.ResourceProvider
import la.me.leo.tibber.domain.FetchPowerUpUseCase
import la.me.leo.tibber.presentation.models.Header
import la.me.leo.tibber.presentation.models.PowerUp
import la.me.leo.tibber.presentation.models.PowerUpListItemModel

class PowerUpListViewModel(
    private val fetchPowerUpUseCase: FetchPowerUpUseCase,
    private val resourceProvider: ResourceProvider
) : BaseViewModel<PowerUpListVS>(PowerUpListVS.LoadingList) {
    init {
        viewModelScope.launch {
            try {
                val (active, inactive) = fetchPowerUpUseCase.execute()
                    .map { PowerUp(it) }
                    .partition { it.powerUp.connected }
                val uiModels = listOf(
                    if (active.isNotEmpty()) listOf(Header(resourceProvider.getString(R.string.powerup_list_active))) else emptyList(),
                    active,
                    if (inactive.isNotEmpty()) listOf(Header(resourceProvider.getString(R.string.powerup_list_inactive))) else emptyList(),
                    inactive
                ).flatten()
                _viewStates.value = PowerUpListVS.ListLoaded(uiModels)
            } catch (t: Throwable) {
                _viewStates.value = PowerUpListVS.Failure
            }
        }
    }
}

sealed class PowerUpListVS: BaseViewState {

    object LoadingList: PowerUpListVS()
    data class ListLoaded(val items: List<PowerUpListItemModel>): PowerUpListVS()
    object Failure: PowerUpListVS()

}
