package la.me.leo.tibber.domain

import org.koin.dsl.module

val domainModule = module {
    factory<FetchPowerUpUseCase> { FetchPowerUpUseCaseImpl(get()) }
}
