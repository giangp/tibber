package la.me.leo.tibber

import android.app.Application
import la.me.leo.tibber.common.commonModule
import la.me.leo.tibber.domain.domainModule
import la.me.leo.tibber.presentation.presentationModule
import la.me.leo.tibber.remote.remoteModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TibberApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@TibberApp)
            modules(commonModule, remoteModule, domainModule, presentationModule)
        }
    }
}
