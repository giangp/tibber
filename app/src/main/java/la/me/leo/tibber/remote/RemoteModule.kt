package la.me.leo.tibber.remote

import org.koin.dsl.module

val remoteModule = module {
    single { buildOkHttpClient(emptyList(), emptyList()) }
    single { buildGraphQLClient(baseUrl = "https://app.tibber.com/v4/gql", okHttpClient = get()) }
}
