package la.me.leo.tibber.base

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.viewbinding.ViewBinding


internal abstract class StatefulFragment<VM : BaseViewModel<VS>, VS: BaseViewState, B : ViewBinding> :
    BaseFragment<B>() {

    protected abstract val viewModel: VM

    abstract fun render(viewState: VS)

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewStates.observe(viewLifecycleOwner, ::render)
    }
}
