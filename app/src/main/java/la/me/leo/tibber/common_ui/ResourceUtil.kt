package la.me.leo.tibber.common_ui

import android.content.Context
import android.util.TypedValue

fun dp(dp: Float, context: Context): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics).toInt()
}

fun dp(dp: Int, context: Context) = dp(dp.toFloat(), context)
