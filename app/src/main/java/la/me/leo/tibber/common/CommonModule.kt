package la.me.leo.tibber.common

import org.koin.dsl.module

val commonModule = module {
    single<ResourceProvider> { ResourceProviderImpl(get()) }
}
