package la.me.leo.tibber

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import androidx.transition.Fade
import androidx.transition.Fade.OUT
import androidx.transition.Slide
import la.me.leo.tibber.domain.models.PowerUp
import la.me.leo.tibber.ui.PowerUpDetailFragment
import la.me.leo.tibber.ui.PowerUpListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.flRoot, PowerUpListFragment())
                .commit()
        }
    }

    fun navigateToPowerUpDetail(powerUp: PowerUp) {
        val fragment = PowerUpDetailFragment.newInstance(powerUp)
        fragment.enterTransition = Slide(Gravity.END)
        fragment.returnTransition = Fade(OUT)
        supportFragmentManager.beginTransaction()
            .add(R.id.flRoot, fragment)
            .addToBackStack(null)
            .commit()
    }
}
