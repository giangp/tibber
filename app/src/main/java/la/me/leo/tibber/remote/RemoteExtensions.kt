package la.me.leo.tibber.remote

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Error
import com.apollographql.apollo.api.Operation
import com.apollographql.apollo.api.Query
import com.apollographql.apollo.coroutines.await
import okhttp3.Interceptor
import okhttp3.OkHttpClient

fun buildGraphQLClient(
    baseUrl: String,
    okHttpClient: OkHttpClient
): ApolloClient {
    return ApolloClient.builder()
        .serverUrl(baseUrl)
        .okHttpClient(okHttpClient)
        .build()
}

fun buildOkHttpClient(
    restApiInterceptors: List<Interceptor>?,
    restApiNetworkInterceptors: List<Interceptor>?
): OkHttpClient {
    return OkHttpClient.Builder()
        .apply {
            restApiInterceptors?.forEach { addInterceptor(it) }
            restApiNetworkInterceptors?.forEach { addNetworkInterceptor(it) }
        }
        .build()
}

suspend fun <D : Operation.Data, T, V : Operation.Variables> ApolloClient.queryResult(q: Query<D, T, V>): T {
    val r = query(q).await()
    return r.data ?: throw r.errors?.let {  ApolloException(it) } ?: RuntimeException("Unexpected exception")
}

class ApolloException(val errors: List<Error>): RuntimeException()
